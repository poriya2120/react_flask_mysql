import axios from 'axios'
 

export const getList=()=>{
     return axios
        .get('api/tasks',{
            headers:{"content-type":"application/json"}

        })
        .then(res =>{
            var data=[]
            Object.keys(res.data).forEach((key)=>{
                var val=res.data[key]
                data.push([val.title,val.id])
                
            })
        })
 }
 export const addTolist=(term)=>{
    return axios
       .post('api/tasks',{
           title:term
       },{
           headers:{"Content-type":"application/json"}
       }
       )
       .then((res) =>{
          console.log(res)
               
          
       })
}


export const deleteItem=(term)=>{
    return axios
       .delete(`api/tasks ${term}`,{
           headers:{"Content-type":"application/json"}
       }
       )
       .then((res) =>{
          console.log(res)
               
          
       })
       .catch((res)=>{
           console.log(res)
       })
}

export const updateItem=((term,id) =>{
    axios
    .put(`api/task/${id}`,{
        title:term
    },{
        headers:{"Content-type":"application/json"}
    })
    .then((res)=>{
        console.log(res)
    })
})