import React, { Component } from 'react';
import {getList,addTolist,deleteItem,updateItem} from './ListFunctions'

class List extends Component{
    constructor(){
        super()
        this.state={
            id:'',
            term:'',
            editDisabled:false,
            items:[]
        }
        this.onSubmit=this.onSubmit.bind(this)
        this.onSubmit=this.onChange.bind(this)

    }
    componentDidMount(){
        this.getAll()
    }
    onChange=e=>{
        this.getSnapshotBeforeUpdate({
            term:e.target.value,
            editDisabled:'disabled'
        })
    }

    getAll=()=>{
        getList().then(data=>{
            this.setState({
                term:'',
                items:[...data]
            },
            ()=>{
                console.log(this.state.term)
            }
            )
        })
    }
   onSubmit=e=>{
       e.preventDefault()
       this.setState({editDisabled:''})
       addTolist(this.state.term).then(()=>{
           this.getAll()
       })
   } 

   onUpdate=e=>{
    e.preventDefault()
    this.setState({editDisabled:''})
    updateItem(this.state.term,this.state.id).then(()=>{
        this.getAll()
    })
} 

    onEdit=(items,itemId,e)=>{
        e.preventDefault()
        this.setState({
            id:itemId,
             term:term
        })
        console.log(itemId)
    }
    
    onDelete=(val,e)=>{
        e.preventDefault()
        deleteItem(val)
        var data=[...this.state.items]
        data.filter((item,index)=>{
            if(item[1]===val){
                data.splice(index,1)
            }
            return true
        })
        this.setState({items :[...data]})
    }

    render(){
        return<>
        
        </>
    }
}